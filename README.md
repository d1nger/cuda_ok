# README #

CUDA join ordering opitmization

### What is this repository for? ###

* An efficient way to accelerate join ordering in DBMS optimization part using CUDA parallel programming
* Version 1.0

### How do I get set up? ###

* Windows : Configure Visual Studio 2010 with Nvidia compiler, clone and run
* Linux : Download nvgcc compiler, clone, run source